import xml.etree.ElementTree as E_Tree
import datetime as dt

NOW = dt.datetime.now()
tree = E_Tree.parse('country_data.xml')
root = tree.getroot()

# Parse xml to dictionary
c_dict = {}
# c => country
for c in root.findall('country'):
    c_dict[c.get('name')] = {
        'rank': int(c.find('rank').text),
        'year': int(c.find('year').text),
        'gdppc': int(c.find('gdppc').text),
        'neighbor': c.find('neighbor').get('name')
    }

# Create csv if not exist
file_name = f'generated-file-{NOW.year}-{NOW.month}-{NOW.day}_{NOW.hour}.csv'
with open(file=file_name, mode='w', encoding='utf-8') as f:
    f.write('name;rank;year;gdppc;neighbor\n')
    for c in c_dict:
        f.write(f'"{c}";{c_dict[c]["rank"]};{c_dict[c]["year"]};{c_dict[c]["gdppc"]};"{c_dict[c]["neighbor"]}"\n')
